const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const httpError = require("http-errors");
// const swaggerUi = require("swagger-ui-express");
// const swaggerDocument = require("./swagger.json");
const routes = require("../routes/index.route");
const app = express();

// set angular to use express as server
var distDir = "../../dist/mean-stack-template";
app.use(express.static(path.join(__dirname, distDir)));
app.use(/^((?!(api)).)*/, (req, res) => {
  res.sendFile(path.join(__dirname, distDir + "/index.html"));
});

// express config
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// enable CORS - Cross Origin Resource Sharing
app.use(cors());

// swagger ui
// app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// API routes
app.use("/api/", routes);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new httpError(404);
  return next(err);
});

// error handler, send stacktrace only during development
app.use((err, req, res, next) => {
  // customize Joi validation errors
  if (err.isJoi) {
    err.message = err.details.map((e) => e.message).join("; ");
    err.status = 400;
  }

  res.status(err.status || 500).json({
    message: err.message,
  });
  next(err);
});

module.exports = app;
