require("dotenv").config();

const config = {
  env: process.env.NODE_ENV,
  port: process.env.SERVER_PORT,
  mongooseDebug: process.env.MONGOOSE_DEBUG || false,
  jwtSecret: process.env.JWT_SECRET,
  mongo: {
    host: process.env.MONGO_HOST,
  },
};

module.exports = config;
