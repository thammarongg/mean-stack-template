const User = require("../models/user.model");

async function insert(user) {
  return await new User(user).save();
}

async function get() {
  const filter = {};
  return await User.find(filter);
}

async function getWithFilter(filter) {
  if (filter === undefined) throw new Error("filter must be defined");
  return await User.find(filter);
}

module.exports = {
  insert,
  get,
  getWithFilter,
};
