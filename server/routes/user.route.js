const express = require("express");
const asyncHandler = require("express-async-handler");
const userController = require("../controllers/user.controller");

const router = express.Router();

router.route("/").post(asyncHandler(insert));
router.route("/").get(asyncHandler(get));
router.route("/filter").get(asyncHandler(getWithFilter));

async function insert(req, res) {
  let user = await userController.insert(req.body);
  res.json(user);
}

async function get(req, res) {
  let user = await userController.get();
  res.json(user);
}

async function getWithFilter(req, res) {
  let user = await userController.getWithFilter(req.query);
  res.json(user);
}

module.exports = router;
