# MEAN Stack Template

This project is preset for MEAN starter.

## Installation

 1. Clone project
 2. Run `npm install`
 3. Rename .env.example -> .env
 4. Set up environment variables in .env file
 5. Run `npm start`
 6. You can access Angular web via [http://localhost:4040](http://localhost:4040) and API via [http://localhost:4040/api/](http://localhost:4040/api/)
